#Project 1
##Timothy Carver
##LIS4369
 
1. Backward-engineer (using Visual Studio Community Edition) the following console
application screenshot:
2. Requirements (see below screenshot):
a. Display short assignment requirements
b. Display *your* name as “author”
c. Display current date/time (must include date/time, your format preference)
d. Must perform and display room size calculations, must include data validation
and rounding to two decimal places.
e. Each data member must have get/set methods, also GetArea and GetVolume
 ![Project1.png](https://bitbucket.org/repo/x5oB9j/images/2606298500-Project1.png)